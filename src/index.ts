
import type { HttpFunction } from '@google-cloud/functions-framework';
import Logging from './Logging';
const labelKey = 'logging.googleapis.com/labels';

export const log: HttpFunction = (req, res) => {
  // Check POST
  if (req.method !== 'POST') {
    res.status(405).json({
      message: 'Method not allowed.'
    });
    return;
  }
  // Check token
  if (req.header('x-io-kevinwilliams-token') !== 'applesauce') {
    res.status(403).send();
    return;
  }
  // Check Content-Type
  if (req.header('content-type') !== 'application/json') {
    res.status(400).json({
      message: 'Expected Content-Type to be application/json'
    });
    return;
  }
  // Check the shape of the body
  if (!Logging.isRequestBody(req.body)) {
    res.status(400).json({
      message: 'Invalid body'
    });
    return;
  }

  let { severity, app, device, os, timestamp, message, data, errorClass } = req.body.params as Logging.RequestParams;
  let { id: appId, version: appVersion } = app;
  let { name: osName, version: osVersion } = os;
  let { make: deviceMake, model: deviceModel } = device;
  
  let logEntry = {
    severity,
    timestamp,
    [labelKey]: {
      appId,
      appVersion,
      osName,
      osVersion,
      deviceMake,
      deviceModel,
      errorClass
    },
    message,
    data
  };

  console.log(JSON.stringify(logEntry));
  res.status(200).send();
}