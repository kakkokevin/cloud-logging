
namespace Logging {

  // https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry#LogSeverity
  export enum Severity {
    Default = "DEFAULT",
    Debug = "DEBUG",
    Info = "INFO",
    Notice = "NOTICE",
    Warning = "WARNING",
    Error = "ERROR",
    Critical = "CRITICAL",
    Alert = "ALERT",
    Emergency = "EMERGENCY",
  }

  export interface RequestBody {
    resource: 'io:kevinwilliams:cloud-logging:request';
    version: '1.0.0';
    params: RequestParams;
  }

  export interface RequestParams {
    severity: Severity;
    app: { id: string, version: string };
    os: { name: string, version: string };
    device: { make: string, model: string };
    timestamp?: string;
    message?: string;
    data?: any;
    errorClass?: string;
  }

  export function isRequestBody(body: RequestBody): body is RequestBody {
    return body.resource === 'io:kevinwilliams:cloud-logging:request'
      && body.version === '1.0.0'
      && isRequestParams(body.params);
  }

  export function isRequestParams(params: RequestParams): params is RequestParams {
    return params
      && params.severity && Array.from(Object.values(Severity)).includes(params.severity)
      && params.app?.id && params.app?.version
      && params.os?.name && params.os?.version
      && params.device?.make && params.device?.model
      && (params.timestamp ? !isNaN(new Date(params.timestamp).getDate()) : true)
      && (params.message || params.data || params.errorClass)
  }
}

export default Logging;